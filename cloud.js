/* -----------------------------------  */
class Point {
  constructor(x, y) { this.x = x; this.y = y; }
  coord2str(offset) {
    var x= this.x + offset.x;  var y= this.y + offset.y;
    return(x.toString() + "," + y.toString());  
  }
}
// https://www.petercollingridge.co.uk/tutorials/svg/interactive/dragging/ 
function getMousePosition(element) {
  var CTM = svg.getScreenCTM();
  return {
    x: (element.clientX - CTM.e) / CTM.a,
    y: (element.clientY - CTM.f) / CTM.d
  };
}
/* -----------------------------------  */
function manyClouds() {
  //console.log(svg.clientWidth.toString()+', '+svg.clientHeight.toString());
  var varr1= 15; var varr2= 7; // 10  -5 
  var i= 0;
  for (const [key, value] of Object.entries(maMap)) {
    //console.log(key, value.type);
    i+=1;  if (i>1000) break;
    var offset= new Point(value.x, value.y);//(xo, yo)
    var start= new Point(30 + Math.round(value.randomVector[1] * varr1) -varr2, 50 + Math.round(value.randomVector[2] * varr1) -varr2);
    var ny= 30 + Math.round(value.randomVector[2] * varr1) -varr2;
    var step1= new Point(50 + Math.round(value.randomVector[3] * varr1) -varr2, ny);
    var step2= new Point(80 + Math.round(value.randomVector[4] * varr1) -varr2, ny +2);
    var ny= 62 + Math.round(value.randomVector[5] * varr1) -varr2
    var step3= new Point(85 + Math.round(value.randomVector[6] * varr1) -varr2, ny);
    var step4= new Point(45 + Math.round(value.randomVector[7] * varr1) -varr2, ny);
    var points= [start,step1,step2,step3,step4];
    var red= Math.round(value.randomVector[8] * 50);
    var green= Math.round(value.randomVector[9] * 100) + 155;
    var blue= Math.round(value.randomVector[10] * 50);
    var fill_color= "rgb("+red.toString()+","+green.toString()+","+blue.toString()+")";//"rgb(0,255,0)"
    var id_bush= key; //points[0].coord2str(offset);
    //if (first_bush_id == '') first_bush_id= id_bush;
    createCloud(points, id_bush, offset, fill_color);
  }
}
/* -----------------------------------  */
function createCloud(points, id_bush, offset, fill_color) {
  var use= document.createElementNS(svgns, "use");
  use.setAttribute("x", points[0].x+offset.x);
  use.setAttribute("y", points[0].y+offset.y);
  var strPath= "M "+points[0].coord2str(offset);
  for(p = 1, l = points.length; p < l; p++) {
    strPath+= " A 10,10 0,0,1 "+points[p].coord2str(offset);
  }
  strPath+= " A 10,10 0,0,1 "+points[0].coord2str(offset);
  drawCloudPath(strPath, id_bush, fill_color);//, use);
}
/* -----------------------------------  */
function drawCloudPath(strPath, path_id, fill_color) {
  var path = document.createElementNS(svgns, "path");
  path.setAttributeNS(null, "d", strPath); 
  path.setAttributeNS(null, "fill", fill_color); 
  path.id= path_id;
  //path.setAttributeNS(null, "filter", "blur(1px)"); 
  //path.setAttributeNS(null, "filter", "url(#blurMe)"); 
  path.addEventListener('mousedown', mousedown_CloudPath);
  path.addEventListener('mousemove', mousemove_CloudPath);
  path.addEventListener('mouseup', mouseup_CloudPath);
  path.addEventListener('mouseout', mouseout_CloudPath);
  grp_path.appendChild(path);
}
/* -----------------------------------  */
/*function load_drawCloudPath(element) {
  console.log("load_drawBushPath ");
}*/
/* -----------------------------------  */
function mouseup_CloudPath(element) {
  console.log("mouseup_CloudPath ");
  if (! selectedCloud) return; 
  //selectedCloud = element.target;
  var idCloud= selectedCloud.getAttribute('id');
  maMap[idCloud].mx= lastCloudTranslate.x;
  maMap[idCloud].my= lastCloudTranslate.y;
  selectedCloud= false;
}
/* -----------------------------------  */
function mouseout_CloudPath(element) {
  if (! selectedCloud) return; 
  var idCloud= selectedCloud.getAttribute('id');
  maMap[idCloud].mx= lastCloudTranslate.x;
  maMap[idCloud].my= lastCloudTranslate.y;  
  selectedCloud= false;
}
/* -----------------------------------  */
function mousemove_CloudPath(element) {
  if (! selectedCloud) return; 
  //element.preventDefault();// which blocks any other dragging behaviour
  var coord = getMousePosition(element);
  //selectedCloud.setAttribute('transform', 'translate(4,4)');
  var idCloud= selectedCloud.getAttribute('id');
  //console.log("id= "+idCloud);
  var cloudMx= parseInt(maMap[idCloud].mx);
  var cloudMy= parseInt(maMap[idCloud].my);
  //console.log("coord.x= "+coord.x+" offsetCloud.x= "+offsetCloud.x+" cloudMx= "+cloudMx);
  var xTranslate= coord.x - offsetCloud.x + cloudMx;//offsetBase.x;
  var yTranslate= coord.y - offsetCloud.y + cloudMy;//offsetBase.y;
  console.log("xTranslate= "+xTranslate+" yTranslate= "+yTranslate);
  selectedCloud.setAttribute('transform', 'translate('+xTranslate.toString()+','+yTranslate.toString()+')');
  lastCloudTranslate= {x:xTranslate,y:yTranslate};
}
/* -----------------------------------  */
function mousedown_CloudPath(element) {
  selectedCloud= element.target;
  console.log("mousedown_CloudPath ");
  offset = getMousePosition(element);
  offsetCloud = getMousePosition(element); // save mouse origin
  console.log("x= "+offset.x.toString());
  console.log("id= "+selectedCloud.getAttribute('id'));
  lastCloudTranslate= {x:0,y:0};
}
/* -----------------------------------  */
/*function bgcol_mousemovettt(element) {
  //return; offsetBase
  if (! selectedBgcol) return; // if not mousedown exit 
  //selectedBgcol = element.target;
  var coord = getMousePosition(element);
  grp_path.setAttribute('transform', 'translate('+(coord.x - offsetBgcol.x + offsetBase.x).toString()+','+(coord.y - offsetBgcol.y + offsetBase.y).toString()+')');
  lastCloudTranslate= {x:(coord.x - offsetBgcol.x + offsetBase.x),y:(coord.y - offsetBgcol.y + offsetBase.y)};
}*/
/* -----------------------------------  */




