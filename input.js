/* -----------------------------------  */
function createInput() {
  // https://stackoverflow.com/questions/55425694/how-can-i-make-svg-text-editable
  group = document.createElementNS(svgns, 'g');
  svg.appendChild(group);
  var foreigner = document.createElementNS(svgns, "foreignObject");
  foreigner.setAttributeNS(null, "x", window.innerWidth/2);//100
  foreigner.setAttributeNS(null, "y", 100);//window.innerHeight-150
  foreigner.setAttributeNS(null, "width", 350);
  foreigner.setAttributeNS(null, "height", 40);
  group.appendChild(foreigner);
  var txt = document.createElement('input');
  txt.setAttributeNS(null, "id", "inputCmd");
  var txtStyle= "background-color:#14a31c;border: 1px solid #023905;border-radius: 4px;outline: none;opacity: 0.5;";
  txt.setAttributeNS(null, "style", txtStyle);
  txt.setAttributeNS(null, "size", 40);
  txt.setAttributeNS(null, "onkeypress", "inputKeyPress(event)");
  foreigner.appendChild(txt);
  var foreigner2 = document.createElementNS(svgns, "foreignObject");
  foreigner2.setAttributeNS(null, "x", window.innerWidth/2);//100
  foreigner2.setAttributeNS(null, "y", 130);//window.innerHeight-150
  foreigner2.setAttributeNS(null, "width", 350);
  foreigner2.setAttributeNS(null, "height", 240);
  group.appendChild(foreigner2);
  var textarea = document.createElement('textarea');
  textarea.setAttributeNS(null, "id", "outputArea");
  var areaStyle= "background-color:#14a31c;border: 1px solid #023905;border-radius: 4px;outline: none;opacity: 0.5;";
  textarea.setAttributeNS(null, "style", areaStyle);
  textarea.setAttributeNS(null, "rows", 4);
  textarea.setAttributeNS(null, "cols", 41);
  foreigner2.appendChild(textarea);
}
function inputKeyPress(event) {
  if(event.keyCode === 13) {
    event.preventDefault(); // Ensure it is only this code that runs
    var outputArea = document.getElementById("outputArea");
    var inputCmd = document.getElementById("inputCmd");
    var cmd = inputCmd.value;
    switch (cmd) {
      case 'saveMap':
        saveMap(outputArea);
        break;
      case 'delMap':
        delMap(outputArea);
        break;
      /*case 'savedb':
        savePouchdb(outputArea);
        break;*/
      case 'help':
        outputArea.value+= 'help'+ ' \n ';// console.log('help');
        break;
      case 'fefe':
      case 'sese':
        outputArea.value+= 'fefe sese'+ ' \n ';// console.log('fefe sese');
        break;
      default:
        outputArea.value+= 'unkonwn'+ cmd + ' \n ';// console.log('fefe sese');
        //outputArea.value+= `unkonwn ${cmd}.`;// console.log(`unkonwn ${cmd}.`);
    }
    //console.log('Enter was pressed, value is : ' + cmd);
  }
}
/* -----------------------------------  */
function savePouchdb(outputArea) {
  db.get('myMap', function(err, doc) {
    if (err) { 
      outputArea.value+= 'Error while db.get myMap : '+err + ' \n ';//console.log(err); 
      return console.log(err); 
    }
    db.put({
      _id: 'myMap',
      _rev: doc._rev,
      randVector: randVector,
      test: "yes test",
      age: 3,
      startMap: {x:-6000,y:-6000}
    }, function(err, response) {
      if (err) { 
        outputArea.value+= 'Error while db.put myMap : ' + err + ' \n ';//console.log(err); 
        return console.log(err); 
      }
      // handle response
      outputArea.value+= 'Save myMap successfully\n';//console.log('Save myMap successfully');
    });
  });
}

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */

/* -----------------------------------  */


