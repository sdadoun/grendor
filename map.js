/* -----------------------------------  */
function delMap(outputArea) {
  db.get('myMap').then(function (doc) {
    outputArea.value+= 'delMap start remove doc ' + ' \n'; 
    return db.remove(doc);
  }).catch(function (err) {
    console.log('delMap err1 '+err);
  });
}
/* -----------------------------------  */
function syncMyMap() {
  var i= 0;
  for (const [key, value] of Object.entries(maMap)) {
    //console.log(key, value.type);
    i+=1;  if (i>1000) break;
    value.x+= value.mx;  value.mx= 0;
    value.y+= value.my;  value.my= 0;
  }
}
/* -----------------------------------  */
function saveMap(outputArea) {
  syncMyMap();
  db.get('myMap', function(err, doc) {
    var revi= ''; 
    if (err) { 
      outputArea.value+= 'saveMap err1 '+err + ' \n'; 
      console.log('saveMap err1 '+err); 
    } else { revi= doc._rev; }
    db.put({
      _id: 'myMap',
      _rev: revi,
      "maMap": maMap,
      "test": "yes test",
      "age": 3,
      "startMap": {x:-6000,y:-6000}
    }, function(err, response) {
      if (err) { 
        outputArea.value+= 'saveMap err2 '+err + ' \n'; 
        return console.log('saveMap err2 '+err); 
      }
      // handle response
      console.log('Save myMap successfully');
      outputArea.value+= 'Save myMap successfully' + ' \n'; 
    });
  });
}
/* -----------------------------------  */
function randomMap() {
  //var consoleLog= '';
  for(var i = 0; i < 500; i+=1) {
    var nname= RandomName(3, 4);
    maMap[nname]= { 
      type: 'cloud',
      x: Math.round((Math.random()-0.5)*3000),
      y: Math.round((Math.random()-0.5)*3000),
      mx: 0,
      my: 0,
      randomVector: [Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random() ] 
    }; 
    //consoleLog+= '  '+nname;
  }
  //console.log(consoleLog);
}

