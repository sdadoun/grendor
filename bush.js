    /* -----------------------------------  */
    class Point {
      constructor(x, y) { this.x = x; this.y = y; }
      coord2str(offset) {
        var x= this.x + offset.x;  var y= this.y + offset.y;
        return(x.toString() + "," + y.toString());  
      }
    }
    /* -----------------------------------  */
    function manyBushes() {
      createRandVector();
      var varr1= 15; var varr2= 7; // 10  -5 
      //console.log(svg.clientWidth.toString()+', '+svg.clientHeight.toString());
      //for(xo = 0; xo < 1000; xo+=60) for(yo= 0; yo < 2600; yo+=60) {
      for(xo = startMap.x; xo < endMap.x; xo+=60) for(yo= startMap.y; yo < endMap.y; yo+=60) {
      //for(xo = -600; xo < svg.clientWidth; xo+=60) for(yo= -600; yo < svg.clientHeight; yo+=60) {
        //if (Math.random()>0.2) continue;
        if (randVector[xo][yo][0]>0.2) continue;
        var offset= new Point(xo, yo);
        var start= new Point(30 + Math.round(randVector[xo][yo][1] * varr1) -varr2, 50 + Math.round(randVector[xo][yo][2] * varr1) -varr2);
        var ny= 30 + Math.round(randVector[xo][yo][2] * varr1) -varr2
        var step1= new Point(50 + Math.round(randVector[xo][yo][3] * varr1) -varr2, ny);
        var step2= new Point(80 + Math.round(randVector[xo][yo][4] * varr1) -varr2, ny +2);
        var ny= 62 + Math.round(randVector[xo][yo][5] * varr1) -varr2
        var step3= new Point(85 + Math.round(randVector[xo][yo][6] * varr1) -varr2, ny);
        var step4= new Point(45 + Math.round(randVector[xo][yo][7] * varr1) -varr2, ny);
        var points= [start,step1,step2,step3,step4];
        var red= Math.round(randVector[xo][yo][8] * 50)
        var green= Math.round(randVector[xo][yo][9] * 100) + 155
        var blue= Math.round(randVector[xo][yo][10] * 50)
        var fill_color= "rgb("+red.toString()+","+green.toString()+","+blue.toString()+")";//"rgb(0,255,0)"
        var id_bush= points[0].coord2str(offset)
        if (first_bush_id == '') first_bush_id= id_bush;
        createBush(points, id_bush, offset, fill_color);
      }
    }
    function createBush(points, id_bush, offset, fill_color) {
      var use= document.createElementNS(svgns, "use");
      use.setAttribute("x", points[0].x+offset.x);
      use.setAttribute("y", points[0].y+offset.y);
      var strPath= "M "+points[0].coord2str(offset);
      for(p = 1, l = points.length; p < l; p++) {
        strPath+= " A 10,10 0,0,1 "+points[p].coord2str(offset);
      }
      strPath+= " A 10,10 0,0,1 "+points[0].coord2str(offset);
      drawBushPath(strPath, id_bush, fill_color);//, use);
    }
    /* -----------------------------------  */
    function drawBushPath(strPath, path_id, fill_color) {//, use) {
      //var path = document.createElementNS("http://www.w3.org/2000/svg", "path");
      //var grp= document.createElementNS(svgns, "g");
      //grp.id= 'grp_'+path_id;
      var path = document.createElementNS(svgns, "path");
      path.setAttributeNS(null, "d", strPath); //path.setAttributeNS(null, "stroke", color);
      path.setAttributeNS(null, "fill", fill_color); //path.setAttributeNS(null, "stroke-width", "0px");
      path.id= path_id;
      //path.setAttributeNS(null, "filter", "blur(1px)"); 
      //path.setAttributeNS(null, "filter", "url(#blurMe)"); 
      //path.addEventListener('load', load_drawBushPath);
      /*grp.addEventListener('mousedown', mousedown_BushPath);
      grp.addEventListener('mousemove', mousemove_BushPath);
      grp.addEventListener('mouseup', mouseup_BushPath);*/
      path.addEventListener('mousedown', mousedown_BushPath);
      path.addEventListener('mousemove', mousemove_BushPath);
      path.addEventListener('mouseup', mouseup_BushPath);
      //grp.appendChild(path);
      //grp_path.appendChild(grp);
      grp_path.appendChild(path);
      //use.appendChild(path);
      //grp_path.appendChild(use);
    }
    /* -----------------------------------  */
    function load_drawBushPath(element) {
      console.log("load_drawBushPath ");
    }
    /* -----------------------------------  */
    function mouseup_BushPath(element) {
      selectedBush= false;
    }
    /* -----------------------------------  */
    function mousemove_BushPath(element) {
      if (! selectedBush) return; 
      //element.preventDefault();// which blocks any other dragging behaviour
      var x= element.x;  var y= element.y;
      //console.log("use_image_svg_mousemove "+x.toString()+","+y.toString());
      selectedBush = element.target;
      var coord = getMousePosition(element);
      //nok element.setAttributeNS(null, "x", x+5 );
      //nok selectedBush.setAttributeNS(null, "x", x+5 );
      //selectedElement.setAttributeNS(null, "x", coord.x - offset.x);//x + 0.9);
      //selectedElement.setAttributeNS(null, "y", coord.y - offset.y);//y + 0.9);
      //console.log("x= "+coord.x.toString());
      //console.log("x= "+x.toString()+" y= "+y.toString());
      //selectedBush.setAttribute('transform', `matrix(1,0,0,1,${x},${y})`);
      //selectedBush.setAttribute('transform', 'translate('+(coord.x - offsetBgcol.x + offsetBase.x).toString()+','+(coord.y - offsetBgcol.y + offsetBase.y).toString()+')');
      //selectedBush.setAttribute('transform', 'translate('+(x + 1).toString()+','+(y + 1).toString()+')');
      selectedBush.setAttribute('transform', 'translate(1,1)');
    }
    /* -----------------------------------  */
    function mousedown_BushPath(element) {
      selectedBush= element.target;
      console.log("mousedown_drawBushPath ");
      offset = getMousePosition(element);
      /*offset.x -= parseFloat(selectedElement.getAttributeNS(null, "x"));
      offset.y -= parseFloat(selectedElement.getAttributeNS(null, "y"));
      console.log("x= "+selectedElement.getAttributeNS(null, "x").toString());*/
      console.log("x= "+offset.x.toString());
    }
    /* -----------------------------------  */

    /* -----------------------------------  */




