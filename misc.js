/* -----------------------------------  */
function initPouch(callbackOnload) {
  db.get('myMap').then(function (doc) {
    //console.log("myMap then "+doc.age);
    const returnedTarget = Object.assign(randVector, doc.randVector);
    //const returnedTarget = Object.assign(target, source);
    randVector= doc.randVector;
    callbackOnload();
  }).catch(function (err) {
    console.log(err);
  });
}
/* -----------------------------------  */
function afterOnload() {
  body= document.getElementsByTagName("body")[0];
  initSVG();

  bgcol= document.createElementNS(svgns, "rect");
  bgcol.setAttributeNS(null, "width", "100%");//1200);
  bgcol.setAttributeNS(null, "height", "100%");//900);
  bgcol.addEventListener('mousedown', bgcol_mousedown);
  bgcol.addEventListener('mouseup', bgcol_mouseup);
  bgcol.addEventListener('mousemove', bgcol_mousemove);
  bgcol.setAttribute("fill", "#677322");
  //bgcol.setAttributeNS(null, "filter", "blur(100px)"); 
  svg.appendChild(bgcol);
  //console.log(svg.clientWidth.toString()+', '+svg.clientHeight.toString());

  first_bush_id= ''
  grp_path= document.createElementNS(svgns, "g");
  grp_path.id= 'grp_path';
  manyBushes();

  add_assets();

  saveCouch();

  svg.appendChild(grp_path);
  createInput();
 }
/* -----------------------------------  */
function saveCouch() {
  db.get('myMap').then(function (doc) {
    //console.log(doc.age);
  }).catch(function (err) {
    console.log(err);

    var doc = {
      "_id": "myMap",
      "randVector": randVector,
      "test": "yes test",
      "age": 3,
      "startMap": {x:-6000,y:-6000}
    };

    db.put(doc, function callback(err, result) {
      if (!err) {
        console.log('Save myMap successfully');
      } else { console.log(err); }
    });
  });
}

/*var startMap= {x:-6000,y:-6000}; var endMap= {x:6000,y:6000};
function createRandVector() {
  for(xo = startMap.x; xo < endMap.x; xo+=60) {
    randVector[xo]= {};
    for(yo= startMap.y; yo < endMap.y; yo+=60) {
    //for(yo= -600; yo < svg.clientHeight; yo+=60) {
      randVector[xo][yo]= [Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random() ]; 
      //if (xo==0 && yo==0) console.log(randVector.toString());
    }
  }
}*/


/* -----------------------------------  */
//function add_people() {
function add_assets() {
  image_svg("elephant4", "assets/elephant4.svg", "64", "64"); 
  //image_svg("people_1", "assets/people_1.svg", "64", "64"); 
  image_svg("mouse2", "assets/mouse2.svg", "96", "96"); 
  image_svg("mouseB1", "assets/mouseB1.svg", "96", "96"); 
  image_svg("rat2", "assets/rat2.svg", "96", "96"); 
  use_image_svg("#mouse2", 100, 100); 
  use_image_svg("#mouseB1", 100, 200); 
  //use.addEventListener('mouseenter', testlog);
  use_image_svg("#elephant4", 200, 200); 
  //use_image_svg("#people_1", 200, 100); 
  use_image_svg("#rat2", 300, 100); 
} 
/* -----------------------------------  */
function image_svg(grp_id, svg_file, width, height) {
  // https://stackoverflow.com/questions/18492492/how-to-draw-a-svg-in-multiple-locations-on-top-of-another-svg-image
  // append svg <- defs <- grp <- image
  var defs= document.createElementNS(svgns, 'defs');
  var grp= document.createElementNS(svgns, "g");
  grp.id= grp_id;
  var img= document.createElementNS(svgns, "image");
  img.setAttributeNS(xlinkNS, "href", svg_file);
  img.setAttributeNS(null, "width", width);
  img.setAttributeNS(null, "height", height);
  grp.appendChild(img);
  defs.appendChild(grp);
  svg.appendChild(defs);
}
/*function use_image_key2b(grp_id_href, x, y) { 
  var use= document.createElementNS(svgns, "use");
  use.setAttributeNS(xlinkNS, "href", grp_id_href);
  use.setAttribute("bottom", 50);
  use.setAttribute("right", 50);
  //use.setAttribute("x", x);
  //use.setAttribute("y", y);
  svg.appendChild(use);
}*/
function use_image_svg(grp_id_href, x, y) { 
  // create use with the xlinkNS to grp
  var use= document.createElementNS(svgns, "use");
  use.setAttributeNS(xlinkNS, "href", grp_id_href);
  use.setAttribute("x", x);
  use.setAttribute("y", y);
  use.addEventListener('mouseup', use_image_svg_mouseup);
  use.addEventListener('mousedown', use_image_svg_mousedown);
  use.addEventListener('mousemove', use_image_svg_mousemove);
  //use.addEventListener('mouseleave', endDrag);
  grp_path.appendChild(use);
}
function use_image_svg_mouseup(element) {
  selectedElement= false;
}
function use_image_svg_mousedown(element) {
  selectedElement= element.target;
  console.log("use_image_svg_mousedown ");
  offset = getMousePosition(element);
  offset.x -= parseFloat(selectedElement.getAttributeNS(null, "x"));
  offset.y -= parseFloat(selectedElement.getAttributeNS(null, "y"));
}
function use_image_svg_mousemove(element) {
  if (! selectedElement) return; 
  //element.preventDefault();// which blocks any other dragging behaviour
  var x= element.x;  var y= element.y;
  //console.log("use_image_svg_mousemove "+x.toString()+","+y.toString());
  selectedElement = element.target;
  var coord = getMousePosition(element);
  selectedElement.setAttributeNS(null, "x", coord.x - offset.x);//x + 0.9);
  selectedElement.setAttributeNS(null, "y", coord.y - offset.y);//y + 0.9);
  //nok element.style.transform = 'translate(x+10 y+10)';
}
// https://www.petercollingridge.co.uk/tutorials/svg/interactive/dragging/ 
function getMousePosition(element) {
  var CTM = svg.getScreenCTM();
  return {
    x: (element.clientX - CTM.e) / CTM.a,
    y: (element.clientY - CTM.f) / CTM.d
  };
}
/* -----------------------------------  */
function initSVG() {
  // https://stackoverflow.com/questions/20539196/creating-svg-elements-dynamically-with-javascript-inside-html
  svg = document.createElementNS(svgns, "svg");
  svg.setAttributeNS(null, "width", "100%");//1200
  svg.setAttributeNS(null, "height", "100%");
  /*svg.addEventListener('mousedown', bgcol_mousedown);
  svg.addEventListener('mouseup', bgcol_mouseup);
  svg.addEventListener('mousemove', bgcol_mousemove);*/
  body.appendChild(svg);
  // ok ? svg.addEventListener('onload', makeDraggable());
}
function bgcol_mouseup(element) {
  selectedBgcol= false; // quit mousedown
  //offsetBgcol_orig = getMousePosition(element); // not used
  offsetBase= lastTranslate; // save last translation
}
function bgcol_mousedown(element) {
  if (selectedElement) return;// if mousedown over a people exit 
  selectedBgcol= element.target;// mark mousedown
  //console.log('bgcol_mousedown');
  //  first_bush.remove();
  offsetBgcol = getMousePosition(element); // save mouse origin
}

function bgcol_mousemove(element) {
  //return; offsetBase
  if (! selectedBgcol) return; // if not mousedown exit 
  //selectedBgcol = element.target;
  var coord = getMousePosition(element);
  grp_path.setAttribute('transform', 'translate('+(coord.x - offsetBgcol.x + offsetBase.x).toString()+','+(coord.y - offsetBgcol.y + offsetBase.y).toString()+')');
  lastTranslate= {x:(coord.x - offsetBgcol.x + offsetBase.x),y:(coord.y - offsetBgcol.y + offsetBase.y)};
}
/* -----------------------------------  */
var startMap= {x:-6000,y:-6000}; var endMap= {x:6000,y:6000};
function createRandVector() {
  if (typeof randVector !== 'undefined' && randVector.length > 0) {
    // the array is defined and has at least one element
    console.log("randVector"+randVector.toString());
    console.log("randVector.length"+randVector.length.toString());
  }

  /*for(xo = startMap.x; xo < endMap.x; xo+=60) {
    randVector[xo]= {};
    for(yo= startMap.y; yo < endMap.y; yo+=60) {
    //for(yo= -600; yo < svg.clientHeight; yo+=60) {
      randVector[xo][yo]= [Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random(), Math.random() ]; 
      //if (xo==0 && yo==0) console.log(randVector.toString());
    }
  }*/
}

